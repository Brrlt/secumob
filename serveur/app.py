# import the Flask class from the flask module
from flask import Flask, render_template, redirect, url_for, request, session, flash
from functools import wraps
app = Flask(__name__)

app.secret_key = 'supersecret'

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'connect' in session:
            return f(*args, **kwargs)
        else:
            flash('Veuillez vous connecter.')
            return redirect(url_for('login'))
    return wrap

@app.route('/')
@login_required
def home():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != 'user' or request.form['password'] != 'pass':
            error = 'Authentifiants invalides'
        else:
            session['connect'] = True
            flash('Vous êtes connecté.')
            return redirect(url_for('home'))
    return render_template('login.html', error=error)

@app.route('/logout')
@login_required
def logout():
    session.pop('connect', None)
    flash('Vous êtes déconnecté.')
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run(debug=True)