# Projet de Sécurité Mobile - Blackholing/Greyholing
## Master 2 Cybersécurité - Université Paris Cité

Outils pour le déploiement et l'execution des attaques de blackholing et greyholing.

## Architecture

## Serveur web


installer le serveur web :
```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
flask run
```

## Outils